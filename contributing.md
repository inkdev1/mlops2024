## Использование линтера ruff в проекте
Для использования линтера ruff в проекте необходимо его установить
```
pip install ruff
```
а затем воспользоваться команадами
```
ruff check                          # Lint all files in the current directory (and any subdirectories).
ruff check path/to/code/            # Lint all files in `/path/to/code` (and any subdirectories).
```
Конфигурация ruff расположена в файле проекта pyproject.toml