FROM mambaorg/micromamba:1.5.8

WORKDIR /project

COPY env.yml pyproject.toml README.md /project/
COPY src/ /project/src/
COPY tests/ /project/tests/
RUN micromamba create -f env.yml -y && \
    micromamba clean --all --yes
ARG MAMBA_DOCKERFILE_ACTIVATE=1
RUN micromamba run -n mlops2024 pdm install