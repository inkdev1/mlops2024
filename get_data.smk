
rule get_data:
  output:
    "data/raw/ny-2015-street-tree-census-tree-data.zip"
  shell:
    """
    kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data --path data/raw/
    """
    
rule extract_data:
  input:
    "data/raw/ny-2015-street-tree-census-tree-data.zip"
  output:
    "data/raw/2015-street-tree-census-tree-data.csv"
  shell:
    """
    unzip data/raw/ny-2015-street-tree-census-tree-data.zip -d data/raw/
    """

